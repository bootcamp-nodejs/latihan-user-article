const config = require('../util/config');
const db = require('../util/db');
const Role = db.role;
const User = db.user;

checkDuplicateUserNameOrEmail = (req, res, next) => {
    //check username is already in use    
    User.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        
        if (user) {
            res.status(400).json({ fail: "Username is already taken" });
            return;
        }
        //check email is already in use
        User.findOne({
            where: {
                email: req.body.email
            }
        }).then(user => {
            
            if (user) {
                res.status(400).json({ fail: "Email is already im use" });
                return;
            }
            next();
        });
    });
}

checkRolesExisted = (req, res, next) => {

    if (req.body.roles !== undefined) {
        for (let i = 0; i < req.body.roles.length; i++) {
            if (!config.roles.includes(req.body.roles[i].toUpperCase())) {
                res.status(400).json({ fail: "Do not exist role = " + req.body.roles[i] });
                return;
            }
            console.log("middleware du");
        }
        next();
    } else {
        next();
    }
}

const signUpVerify = {};
signUpVerify.checkDuplicateUserNameOrEmail = checkDuplicateUserNameOrEmail;
signUpVerify.checkRolesExisted = checkRolesExisted;

module.exports = signUpVerify;