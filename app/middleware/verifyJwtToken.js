const jwt = require('jsonwebtoken');
const config = require('../util/config');
const db = require('../util/db');
const Role = db.role;
const User = db.user;

verifyToken = (req, res, next) => {
    // console.log("in verify token")
    let token = req.headers["x-access-token"] || req.headers["authorization"];
    // console.log('token: ',token);
    if (!token) {
        console.log('masuk ke !token');
        return res.status(403).json({
            auth: false,
            message: "No token provide"
        });
    }
    // console.log('token: ',typeof token);
    if (token.startsWith("Bearer")) {
        //Remove Bearer from string
        token = token.slice(7, token.length);
    }


    jwt.verify(token, config.secret, (err, decode) => {
        if (err) {
            return res.status(500).json({
                auth: false,
                message: "Fail to authentication. Error -> "+err,
            });
        }
        // console.log('decode: ',decode);
        req.userId = decode.id;
        next();
    });
}

isAdmin = (req, res, next) => {
    // console.log(req);
    User.findByPk(req.userId).then(user => { 
        user.getRoles().then(roles => {
            // console.log("roles: ",typeof roles);
            for (let i = 0; i < roles.length; i++) {
                console.log(roles[i].name);
                if (roles[i].name.toUpperCase() === "ADMIN") {
                    next();
                    return;
                }
            }
            res.status(403).json({ message: "Require admin role!" });
            return;
        });
    });
}

isPmOrAdmin = (req,res,next) => {
    User.findByPk(req.userId).then(user => {
        user.getRoles().then(roles => {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i].name.toUpperCase() === "PM") {
                    next();
                    return;
                }
                if (roles[i].name.toUpperCase() === "ADMIN") {
                    next();
                    return;
                }
            }
            res.status(403).json({message:"Require PM or ADMIN roles"});
        });
    });
}

isUserOrAdmin = (req, res, next) => {
    User.findByPk(req.userId).then(user => {
        user.getRoles().then(roles => {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i].name.toUpperCase() === "USER") {
                    next();
                    return;
                }
                if (roles[i].name.toUpperCase() === "ADMIN") {
                    next();
                    return;
                }
            }
            res.status(403).json({
                status: "failed",
                message: "You are in role PM only!!. You cannot do this action!!, please add your role to ADMIN or USER"
            });
        });
    });
}


const authJwt = {};
authJwt.verifyToken = verifyToken;
authJwt.isAdmin = isAdmin;
authJwt.isPmOrAdmin = isPmOrAdmin;
authJwt.isUserOrAdmin = isUserOrAdmin;

module.exports = authJwt;