const { body, validationResult } = require('express-validator');

const validate = validations => {
    return async (req, res, next) => {
        await Promise.all(validations.map(validation => validation.run(req)));

        const errors = validationResult(req);
        if (errors.isEmpty()) {
            return next();
        }

        res.status(400).json({ errors: errors.array() });
    };
};

exports.valExistSignUp = validate([
    body('name').exists({ checkNull: true, checkFalsy: true })
        .withMessage("Please input field name").bail(),
    body('username').exists({ checkNull: true, checkFalsy: true })
        .withMessage("Please input field username").bail(),
    body('email').exists({ checkNull: true, checkFalsy: true })
        .withMessage("Please input field email").bail(),
    body('password').exists({ checkNull: true, checkFalsy: true })
        .withMessage("Please input field password").bail(),
    body('roles').optional({nullable:true,checkFalsy:true}).isArray()
        .withMessage("Roles field must an array").bail(),
]);

exports.valExistSignIn = validate([
    body('username').exists({ checkNull: true, checkFalsy: true })
        .withMessage("Please input field username").bail(),
    body('password').exists({ checkNull: true, checkFalsy: true })
        .withMessage("Please input field password").bail(),
]);

exports.valTypeFieldSignUp = validate([
    body('name').isString().isAlpha().trim()
        .withMessage("name field must be a string and not contain any number").bail(),
    body('username').isString()
        .withMessage("username field must string").bail(),
    body('email').isEmail().trim()
        .withMessage("email field must be an email").bail(),
   
]);

exports.valTypeFieldSignIn = validate([
    body('username').isString().trim()
        .withMessage("name field must be a string").bail(),
]);

exports.valUserUpdate = validate([
    body('name').if(body('name').exists({ checkNull: true, checkFalsy: true }))
        .trim().isString().withMessage("name field must be a string").bail(),
    body('username').if(body('username').exists({ checkNull: true, checkFalsy: true }))
        .trim().isString().withMessage("name field must be a string").bail(),
    body('email').if(body('email').exists({ checkNull: true, checkFalsy: true }))
        .isEmail().withMessage("email field must be an email").bail(),
]);

exports.valArticleExists = validate([
    body('title').exists().withMessage("please insert title field").bail(),
    body('content').exists().withMessage("please insert title content").bail(),
]);

exports.valUpdateRole = validate([
    body('roles').exists().withMessage("Please insert field roles").bail(),
    body('roles').isArray().withMessage("Roles field must an array").bail()
]);