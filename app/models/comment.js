module.exports = (sequelize, Sequelize) => {
    const Comment = sequelize.define('comments', {
        content: {
            type: Sequelize.TEXT
        }
    });

    return Comment;
}