const express = require('express');
const router = express.Router();

const homeController = require('../controller/home');

router.use(express.json());

router.get('/', homeController.home);

module.exports = router;