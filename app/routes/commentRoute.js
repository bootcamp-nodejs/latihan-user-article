var express = require('express');
var router = express.Router();

const commentController = require("../controller/comment.js");
const authJwt = require("../middleware/verifyJwtToken");


//create comment
router.post('/api/comment/:ar_id', [
    authJwt.verifyToken, authJwt.isUserOrAdmin
], commentController.createComment);

//get comment
router.get('/api/comment/:ar_id', commentController.getComment);

//delete user article
router.delete('/api/comment/:cm_id', [
    authJwt.verifyToken, authJwt.isAdmin
], commentController.deleteComment);

module.exports = router;