
var express = require('express');
var router = express.Router();

const authController = require("../controller/auth.js");
const verifySignUp = require("../middleware/verifySignUp");
const inputValidation = require("../middleware/validation");


//auth
router.post('/api/auth/signup', [
    inputValidation.valExistSignUp,
    inputValidation.valTypeFieldSignUp,
    verifySignUp.checkDuplicateUserNameOrEmail,
    verifySignUp.checkRolesExisted
    ], authController.signup);
    
router.post('/api/auth/signin', [
    inputValidation.valExistSignIn,
    inputValidation.valTypeFieldSignIn,
],authController.signin);

module.exports = router;
