var express = require('express');
var router = express.Router();

const articleController = require("../controller/article.js");
const authJwt = require("../middleware/verifyJwtToken");


//create article
router.post('/api/article/', [
    authJwt.verifyToken,authJwt.isUserOrAdmin
], articleController.createArcticle);

//get all article
router.get('/api/article/', articleController.getAllArticle);

//get user article
router.get('/api/user_article/:ar_id', [
    authJwt.verifyToken
], articleController.getUserArticle);

//update user article
router.put('/api/article/:ar_id', [
    authJwt.verifyToken, authJwt.isUserOrAdmin
], articleController.updateUserArticle);

//delete user article
router.delete('/api/article/:ar_id', [
    authJwt.verifyToken, authJwt.isUserOrAdmin
], articleController.deleteUserArticle);

module.exports = router;