var express = require('express');
var router = express.Router();

const userController = require("../controller/user.js");
const authJwt = require("../middleware/verifyJwtToken");
const inputValidation = require("../middleware/validation");

//get all user
router.get('/api/users', [
    authJwt.verifyToken
], userController.users);

//get 1 user according to roles
router.get('/api/test/user', [
    authJwt.verifyToken
], userController.userContent);

//get user is pm or admin
router.get('/api/test/pm', [
    authJwt.verifyToken, authJwt.isPmOrAdmin
], userController.managementBoard);

//get user is an admin
router.get('/api/test/admin', [
    authJwt.verifyToken, authJwt.isAdmin
], userController.adminBoard);

//update user role from admin
router.put('/api/test/admin/:us_id/update', [
    authJwt.verifyToken, authJwt.isAdmin,inputValidation.valUpdateRole
], userController.adminUpdateRoleUser);

//updateUser
router.put('/api/test/user/profile/:id/update', [
    authJwt.verifyToken,inputValidation.valUserUpdate
], userController.userUpdate);

//user delete
router.delete('/api/test/user/:id/delete', [
    authJwt.verifyToken, authJwt.isAdmin
], userController.userDelete);

module.exports = router;