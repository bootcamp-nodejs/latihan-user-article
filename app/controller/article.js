const db = require('../util/db');
const User = db.user;
const Article = db.article;
const asyncMiddleware = require('express-async-handler');

const Op = db.Sequelize.Op;

exports.createArcticle = asyncMiddleware(async (req, res) => {
    let id = req.userId;
    try {
        const article = await Article.create({
            title: req.body.title,
            content: req.body.content,
            userId: id
        });

        console.log('article: ', article);

        res.status(201).json({
            article: article,
            message: "Article created successfully"
        });
    } catch (err) {
        console.log(err);
    }
});

exports.getAllArticle = asyncMiddleware(async (req, res) => {
    try {
        const article = await Article.findAll({
            attributes: ["id","title", "content"],
            include: {
                model: User,
                attributes:["name"]
            }
        });
        res.status(201).json({
            description:"All Article",
            article: article,
        });
    } catch (err) {
        console.log(err);        
    }
});

exports.getUserArticle = asyncMiddleware(async (req, res) => {
    try {
        let userid = req.userId;
        let id = Number(req.params.id);
        
        if (userid === id) {
            const article = await Article.findAll({
                where: {
                    userId: id
                },
                attributes: ["id", "title", "content"],
                include: {
                    model: User,
                    attributes: ["name"]
                }
            });
            
            return res.status(200).json({
                description: "Your article(s)",
                article: article,
            })
        } else {
            res.status(400).json({ message: "You are not own this article" });
        }
    } catch (err) {
        console.log(err);
    }
});

exports.updateUserArticle = asyncMiddleware(async (req, res) => {
    try {
        let userid = req.userId;
        let ar_id = Number(req.params.ar_id);

        const find = await Article.findByPk(ar_id);
        if (find === null) {
            return res.status(400).json({
                message: "Article not found"
            });
        }
        // console.log('this is find',find.userId);
        if (find.userId === userid) {
            const article = await Article.update(
                {
                    title: req.body.title,
                    content:req.body.content
                },
                {
                where: {
                    [Op.and]:[{id:req.params.ar_id},{userId:userid}]
                },
            });
            return res.status(200).json({
                status:"Success",
                message:"Article updated successfully",
                article:article
            })
        } else {
            res.status(400).json({ message: "You are not own this article" });
        }
    } catch (err) {
        console.log(err);
    } 
});

exports.deleteUserArticle = asyncMiddleware(async (req, res) => {
    try {
        let userid = req.userId;
        let ar_id = Number(req.params.ar_id);

        const find = await Article.findByPk(ar_id);
        if (find === null) {
            return res.status(400).json({
                message: "Article not found"
            });
        }
        if (find.userId === userid) {
            find.destroy();
            return res.status(200).json({
                status:"Success",
                message: "Article deleted successfully",
            })
        } else {
            res.status(400).json({ message: "You are not own this article" });
        }
    } catch (err) {
        console.log(err);
    } 
});