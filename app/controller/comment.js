const db = require('../util/db');
const User = db.user;
const Article = db.article;
const Comment = db.comment;

const asyncMiddleware = require('express-async-handler');


exports.getComment = asyncMiddleware(async (req, res) => {
    try {
        const article = await Article.findByPk(req.params.ar_id, {
            attributes:["title"],
            include: {
                model: Comment,
                attributes: ["id","content"]
            }
        });
        if (!article) {
            return res.status(400).json({
                status: "failed",
                message: "Article not found!!"
            });
        }
        res.status(201).json({
            description: "Article",
            article: article,
        });
    } catch (err) {
        console.log(err);
    }
});

exports.createComment = asyncMiddleware(async (req, res) => {
    let id = req.userId;
    try {
        const article = await Article.findByPk(req.params.ar_id);

        if (!article) {
            return res.status(400).json({
                status: "failed",
                message: "Article not found!!"
            });
        }
        const comment = await Comment.create({
            content: req.body.content
        });
        comment.setUser(id);
        comment.setArticle(article);

        res.status(201).json({
            status: "Success",
            message: "Comment posted"
        });
    } catch (err) {
        console.log(err);
    }
});

exports.deleteComment = asyncMiddleware(async (req, res) => {
    try {
        let cm_id = Number(req.params.cm_id);

        const comment = await Comment.findByPk(cm_id);
        if (!comment) {
            return res.status(400).json({
                status:"failed",
                message: "Comment not found"
            });
        } else {
            comment.destroy();
            return res.status(200).json({
                status: "Success",
                message: "Comment deleted successfully"
            })
        }
    } catch (err) {
        console.log(err);
    }
});