const db = require('../util/db');
const User = db.user;
const Role = db.role;
const asyncMiddleware = require('express-async-handler');
let bcrypt = require('bcryptjs');

const Op = db.Sequelize.Op;

exports.users = asyncMiddleware(async (req, res) => {
    try {
        const user = await User.findAll({
            attributes: ["name", "username", "email"],
            include: [
                {
                    model: Role,
                    attributes: ["id", "name"],
                    through: {
                        attributes: ["userId", "roleId"]
                    }
                }
            ],
        });
        res.status(200).json({
            description: "All User",
            user: user,
        });
    } catch (error) {
        console.log(error);
    }
});
exports.userUpdate = asyncMiddleware(async (req,res) => {
    try {
        console.log('roles ', req.body.roles);
        if (Number(req.params.id) !== req.userId) {
            return res.status(400).json({
                status: "failed",
                messsage: "It's not yourself!!, let the owner update his or herself"
            });
        }
        if (req.body.roles !== undefined) {
            return res.status(400).json({
                status:"failed",
                messsage:"Need Role Super Admin to update roles"
            });
        }
        if (req.body.password !== undefined) {
            return res.status(400).json({
                status: "failed",
                messsage: "Please go to page change password for your safety"
            });
        }
        const find = await User.findByPk(req.params.id);
        if (find.name === 'admin') {
            return res.status(400).json({
                status: "failed",
                message:"Cannot update super admin"
            });
        }
        const user = await User.update({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
        }, {
            where:{
                id:req.userId
            }
        });
        res.status(200).json({
            description: "Update user",
            message:"Success",
            user: user,
        });
    } catch (error) {
        console.log(error);
    }
});

exports.userDelete = asyncMiddleware(async (req, res) => {
    try {
        const user = await User.findByPk(Number(req.params.id));
        if (user.name === 'admin') {
            return res.status(400).json({
                status: "failed",
                messsage: "Cannot delete super admin"
            });
        } else if (!user) {
            return res.status(200).json({
                description: "User not Found!!",
                message: "Deleted Failed",
            });
        }else{
            user.destroy();  
            return res.status(200).json({
                description: "User Deleted",
                message: "Success",
            });
        }
    } catch (error) {
        
    }
});

exports.userContent = asyncMiddleware(async (req, res) => {
    try {
        const user = await User.findOne({
            where: { id: req.userId },
            attributes: ["name", "username", "email"],
            include: [
                {
                    model: Role,
                    attributes: ["id", "name"],
                    through: {
                        attributes: ["userId", "roleId"]
                    }
                }
            ]
        });
        res.status(200).json({
            description: "User Content Page",
            user: user,
            roles: user.roles,
        });
    } catch (error) {
        console.log(error);
    }
    
});

exports.adminBoard = asyncMiddleware(async (req, res) => {
    try {
        const user = await User.findOne({
            where: { id: req.userId },
            attributes: ["name", "username", "email"],
            include: [
                {
                    model: Role,
                    attribute: ["id", "name"],
                    through: {
                        attributes: ["userId", "roleId"]
                    }
                }
            ]
        });
        res.status(200).json({
            description: "Admin Board",
            user: user,
        });
    } catch (error) {
        console.log(error);
    }
});

exports.adminUpdateRoleUser = asyncMiddleware(async (req, res) => {
    try {
        const user = await User.findByPk(req.params.us_id);
        if (user && user.name === 'admin') {
            return res.status(400).json({
                status: "Failed",
                message: "Cannot update yourself!!",
            });
        } else if(user){ 
            const role = await Role.findAll({
                where: {
                    name: {
                        [Op.or]: req.body.roles,
                    }
                }
            });
            await user.setRoles(role);
            return res.status(200).json({
                status:"Success",
                message: "User Roles Updated",
            });    
        } else {
            return res.status(400).json({
                status: "Failed",
                message: "User not Found",
            });
        }
    } catch (error) {
        console.log(error);
    }
});

exports.adminAddUser = asyncMiddleware(async (req, res) => {
    try {
        const { roles } = req.body;
        const user = await User.create({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 8),
        });
        roles = roles.map(role => {
            return role.toUpperCase();
        })
        const rl = await Role.findAll({
            where: {
                name: {
                    [Op.or]: roles,
                }
            }
        });
        await user.setRoles(rl);
        return res.status(201).json({
            status: 'User added successfully!!'
        });
    }
    catch (error) {
        console.log(error)
    }
});


exports.managementBoard = asyncMiddleware(async (req, res) => {
    try {
        const user = await User.findOne({
            where: { id: req.userId },
            attributes: ["name", "username", "email"],
            include: [
                {
                    model: Role,
                    attribute: ["id", "name"],
                    through: {
                        attributes: ["userId", "roleId"]
                    }
                }
            ]
        });
        res.status(200).json({
            description: "Management Board",
            user: user,
        });
    } catch (error) {
        console.log(error);
    }
});
