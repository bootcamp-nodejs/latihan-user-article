const db = require('../util/db');
const config = require('../util/config');
const User = db.user;
const Role = db.role;
const asyncMiddleware = require('express-async-handler');

const Op = db.Sequelize.Op;
    
let jwt = require('jsonwebtoken');
let bcrypt = require('bcryptjs'); 

exports.signup = asyncMiddleware(async (req, res) => {
    //Save user to database
    console.log("Processing func -> Sign Up");

    try {
        console.log('body', req.body);
     
        const user = await User.create({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 8),
        });
        const roles = ["USER"];

        console.log('role', roles);
        
        const rl = await Role.findAll({
            where: {
                name: {
                    [Op.or]: roles,
                }
            }
        });
        await user.setRoles(rl);
        return res.status(201).json({
            status:'User register successfully!!'
        });
    }
    catch (error) {
        console.log(error)
        res.status(400).json({
            status: 'failed',
            message: "Unsuccessfull register"
        });
    }
})

exports.signin = asyncMiddleware(async (req,res) => {
    console.log("Sign in");
    try {
        const user = await User.findOne({
            where: {
                username:req.body.username
            },
        });
        // console.log(user);
        if (!user) {
            return res.status(404).json({
                auth: false,
                accessToken: null,
                reason: "User not found"
            });
        }
    
        const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

        const roles = await user.getRoles();
        const user_roles = roles.map(role => {
            return role.name;
        });
        if (!passwordIsValid) {
            return res.status(401).json({
                auth: false,
                accessToken: null,
                reason: "User not found"
            });
        }
        console.log("user_roles ",user_roles);
        const token = jwt.sign({ id: user.id }, config.secret, {
            expiresIn:86400 //expires in 24 hours
        });
        res.status(200).json({
            auth: true,
            type: "Bearer",
            accessToken: token,
            userId: user.id,
            roles:user_roles,
        });
    }
    catch (error) {
        console.log(error);
    }
});