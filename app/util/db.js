const env = require('./env.js');

const { Sequelize } = require('sequelize');
const e = require('express');

const sequelize = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
}); 

const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.user = require('../models/user.js')(sequelize, Sequelize);
db.role = require('../models/role.js')(sequelize, Sequelize);
db.article = require('../models/article.js')(sequelize, Sequelize);
db.comment = require('../models/comment')(sequelize,Sequelize);

db.role.belongsToMany(db.user,{through: 'user_roles',foreginKey:'roleId',otherKey:'userId'});
db.user.belongsToMany(db.role, { through: 'user_roles', foreginKey: 'userId', otherKey: 'roleId' });

db.user.hasMany(db.article);
db.article.belongsTo(db.user,{foreginKey:'userId'});

db.user.hasMany(db.comment);
db.article.hasMany(db.comment);
db.comment.belongsTo(db.user, { foreginKey: 'userId' });
db.comment.belongsTo(db.article, { foreginKey: 'articleId' });

module.exports = db;