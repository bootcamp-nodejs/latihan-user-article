const env = {
    database: 'latihan-node-express',
    username: 'postgres',
    password: 'root',
    host: 'localhost',
    dialect: 'postgres',
    NODE_ENV : 'development',
}
// const env = {
//     database: 'd5g6hus7rsmrf7',
//     username: 'cziwmriqcowbar',
//     password: 'bb942ad70087c42d6278596a0dc0f28348c73ef74a0638a8c298c7bbe6d9ac96',
//     host: 'ec2-54-165-164-38.compute-1.amazonaws.com',
//     dialect: 'postgres',
// }
module.exports = env;