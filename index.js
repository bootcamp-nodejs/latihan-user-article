const express = require('express');
const app = express();
const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");
const morgan = require('morgan');
const cors = require('cors');
const env = require('./app/util/env');
let bcrypt = require('bcryptjs'); 


const HomeRoute = require('./app/routes/homeRoute');
const AuthRoute = require('./app/routes/authRoute');
const UserRoute = require('./app/routes/userRoute');
const ArticleRoute = require('./app/routes/articleRoute');
const CommentRoute = require('./app/routes/commentRoute');

app.use(express.json());
app.use(morgan('common'));

// require('./app/routes/router')(app);

const port = process.env.PORT || 3000;

app.use(cors());
app.use([HomeRoute, AuthRoute, UserRoute, ArticleRoute,CommentRoute]);

const options = require('./swagger-user-article.json');
    

// const specs = swaggerJsdoc(options);
app.use(
    "/api-docs",
    swaggerUi.serve,
    swaggerUi.setup(options, { explorer: true })
);

//error handler 404
app.use(function (req, res, next) {
    return res.status(404).json({
        status: 404,
        message: "Not Found"
    });
});

//error handler 500
app.use(function (err, req, res, next) {
    return res.status().json({
        status: 500,
        error: err
    });
});

const db = require('./app/util/db.js');
const Role = db.role;
const User = db.user;

// force: true will drop the table if it already exists (comment this part after first run, to disable migration)
try {
    db.sequelize.sync({ alter: true }).then(() => {
        console.log('Drop and Resync with { alter: true }');
        // initial();
    }).catch(err => {
        console.log(err);
    });
} catch (err) {
    return res.status(500).json({
        error:"server error"
    })
}


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
    console.log(env.NODE_ENV);
});

function initial() {
    const user = User.create({
        name: "admin",
        username: "admin",
        email: "admin@gmail.com",
        password: bcrypt.hashSync("12345678", 8)
    });
    Role.create({
        id: 1,
        name: "USER"
    });
     Role.create({
        id: 2,
        name: "ADMIN"
    });
    Role.create({
        id: 3,
        name: "PM"
    });
    const role = Role.findByPk(2);
    user.setRoles(role);
}